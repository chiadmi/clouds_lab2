import math

def numerical_integration(lower, upper, N):
    deltax = (upper - lower) / N
    result = 0.0

    for i in range(N):
        x = lower + (i + 0.5) * deltax
        result += abs(math.sin(x)) * deltax

    return result

# Test the program for different values of N
intervals = [10, 100, 1000, 10000, 100000, 1000000]

for N in intervals:
    result = numerical_integration(0, math.pi, N)
    print(f"N = {N}: Result = {result}")

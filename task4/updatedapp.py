import azure.functions as func
import json
from num_integration2 import numerical_integration

def main(req: func.HttpRequest) -> func.HttpResponse:
    try:
        # Parse query parameters from the request
        lower = float(req.params.get('lower'))
        upper = float(req.params.get('upper'))

        # Set default values if not provided
        lower = lower if lower else 0
        upper = upper if upper else 1

        N_values = [10, 100, 1000, 10000, 100000, 1000000]
        results = {}

        for N in N_values:
            result = numerical_integration(lower, upper, N)
            results[N] = result

        return func.HttpResponse(json.dumps(results), mimetype="application/json", status_code=200)

    except ValueError:
        return func.HttpResponse("Invalid input. Please provide valid numerical values for 'lower' and 'upper'.", status_code=400)

import pandas as pd
import matplotlib.pyplot as plt

# Load Locust CSV data into a DataFrame
df = pd.read_csv('output_file.csv')

# Extract relevant columns
df = df[['Time (s)', 'Type', 'Name', 'Request Type', 'Number of requests', 'Number of failures']]

# Filter successful requests
successful_requests = df[(df['Type'] == 'request') & (df['Request Type'] == 'GET') & (df['Number of failures'] == 0)]

# Group by time and calculate requests per second
successful_requests_per_sec = successful_requests.groupby('Time (s)').size()

# Plot the graph
plt.plot(successful_requests_per_sec.index, successful_requests_per_sec.values, label='Successful Requests')
plt.xlabel('Time (s)')
plt.ylabel('Requests per second')
plt.title('Task1')
plt.legend()
plt.show()

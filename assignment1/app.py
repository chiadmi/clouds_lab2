from flask import Flask, jsonify
from num_integration import numerical_integration

app = Flask(__name__)


# Define a route for the microservice
@app.route('/numericalintegralservice/<lower>/<upper>', methods=['GET'])
def numerical_integral_service(lower, upper):
    lower = float(lower)
    upper = float(upper)

    N_values = [10, 100, 1000, 10000, 100000, 1000000]
    results = {}

    for N in N_values:
        result = numerical_integration(lower, upper, N)
        results[N] = result

    return jsonify(results)

if __name__ == '__main__':
    app.run(debug=True)

